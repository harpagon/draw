include config.mk

SRC = draw.c
OBJ = ${SRC:.c=.o}

all: options draw

options:
	@echo draw build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

draw: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f draw ${OBJ} draw-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p draw-${VERSION}
	@cp -R LICENSE Makefile config.mk ${SRC} draw-${VERSION}
	@tar -cf draw-${VERSION}.tar draw-${VERSION}
	@gzip draw-${VERSION}.tar
	@rm -rf draw-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f draw ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/draw
	@echo installing manual page to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@cp draw.1 ${DESTDIR}${MANPREFIX}/man1/draw.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/draw.1

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/draw
	@echo removing manual page from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/draw.1

.PHONY: all options clean dist install uninstall cscope

