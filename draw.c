#include <arpa/inet.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <GLFW/glfw3.h>
#include <linmath.h>

#define GL_LITE_IMPLEMENTATION
#include "gl_lite.h"

typedef struct {
	float x;
	float y;
} CamVec;

typedef struct {
	CamVec wsize;  // actual window size
	CamVec vratio; // expected viewport ratio
//	CamVec scale;
	CamVec offset;
} Camera;

typedef enum {
	ELLIPSE,
	DONUT,
	RECT,
	UNKNOWN_CMD,
	UNKNOWN_PROPERTY,
} CmdType;

typedef struct {
	CmdType t;
	float x;
	float y;
	float r1;
	float r2;
	float colr;
	float colg;
	float colb;
	float cola;
	float rot;
} Cmd;

static CamVec camera_get_viewport_size(Camera *camera);
static void camera_center(Camera *camera, CamVec vsize);

static unsigned int compile_shader(const char *src, int type);
static unsigned int compile_program(const char *vertex_shader, const char *fragment_shader);

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
static void error_callback(int error, const char *description);
static void framebuffer_size_callback(GLFWwindow* window, int width, int height);

static void cmd_parse(char *str, size_t len, Cmd *buf);
static void cmds_load();

static void window_open();
static void gl_init();
static void cleanup();
static void draw();

#define PI 3.14159265358979323846264338327950288
#define DEG_2_RAD(d) (d * PI / 180)

#define TITLE "draw"

#define VERTEX_COUNT 4
#define COORDS_PER_VERTEX 2
#define VERTICES_SIZE 16 * sizeof(float) // VERTEX * COORDS_PER_VERTEX * ...
#define RIGHT 1
#define BOTTOM -1
#define LEFT -1
#define TOP 1

#define GLSL(version, shader)  "#version " #version "\n" #shader

#define LEN_ELLIPSE_CMD 7 // "ellipse" // null terminator not counted
#define LEN_DONUT_CMD 5  // "donut"
#define LEN_RECT_CMD 4   // "rect"
#define LEN_COLOR_PROP 5 // "color"
#define LEN_ROTATE_PROP 6 // "rotate"

GLFWwindow *window;

Camera camera = {
	.wsize  = {.x = 480, .y = 600},
	.vratio = {.x = 1,   .y = 1},
//	.scale  = {.x = 1,   .y = 1},
};

// this program is meant as some kind of farbfeld filter
// therefore one screenshot is allowed so that external program (eg. ff2png)
// can easily parse it
int screenshot_taken = 0;

unsigned int VAO, VBO, ellipse_program, donut_program, rect_program;

float vertices[] = {
	//x      y
	RIGHT, BOTTOM,
	RIGHT, TOP,
	LEFT,  TOP,
	LEFT,  BOTTOM,
};

static const char *vertex_shader = GLSL(330 core,
	layout (location = 0) in vec2 vert;
	uniform mat4 transform;
	out vec2 val;
	void main() {
		val = vert;
		gl_Position = transform * vec4(vert.x, vert.y, 0, 1);
	});
static const char *ellipse_fragment_shader = GLSL(330 core,
	in vec2 val;
	uniform vec4 col;
	void main() {
		float sq_dist = dot(val, val);
		float alpha = smoothstep(1, 0.99, sqrt(sq_dist));
		if(sq_dist >= 1) discard;
		/**
		 * well, it seems to work better for large circles
		 * sucks with ellipses
		**/
		gl_FragColor = vec4(col.x, col.y, col.z, col.a * alpha); // edge smoothing
		//gl_FragColor = col; // no edge smoothing
	});
static const char *donut_fragment_shader = GLSL(330 core,
	in vec2 val;
	uniform float sq_r2;
	uniform vec4 col;
	void main() {
		float sq_dist = dot(val, val);
		if(sq_dist >= 1 || sq_dist <= sq_r2) discard;
		/* smoothing edges */
		float r2 = sqrt(sq_r2);
		float dist = sqrt(sq_dist);
		float alpha;
		alpha = smoothstep(1, 0.99, dist);
		alpha *= smoothstep(r2, r2 + 0.01, dist);
		gl_FragColor = vec4(1, 0.5, 0.2, alpha);
		/* ---------------- */
		//gl_FragColor = col;
	});
static const char *rect_fragment_shader = GLSL(330 core,
	in vec2 val;
	uniform vec4 col;
	void main() {
		gl_FragColor = col;
	});

mat4x4 transf;

size_t cmd_count;
Cmd *cmds;

static CamVec
camera_get_viewport_size(Camera *camera)
{
	CamVec result;
	float wratio = camera->wsize.x / camera->wsize.y;
	float vratio = camera->vratio.x / camera->vratio.y;

	if(wratio > vratio) {
		result.x = camera->wsize.y * vratio;
		result.y = camera->wsize.y;
	} else {
		result.x = camera->wsize.x;
		result.y = camera->wsize.x / vratio;
	}

//	w = w * camera->scale.x;
//	h = h * camera->scale.y;

	return result;
}

static void
camera_center(Camera *camera, CamVec vsize)
{
	camera->offset.x = (camera->wsize.x - vsize.x) / 2;
	camera->offset.y = (camera->wsize.y - vsize.y) / 2;
}

static unsigned int
compile_shader(const char *src, int type)
{
	unsigned int result = glCreateShader(type);
	glShaderSource(result, 1, (const char*const*)&src, NULL);
	glCompileShader(result);
#ifdef DEBUG
	int success;
	char info_log[512];
	glGetShaderiv(result, GL_COMPILE_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(result, 512, NULL, info_log);
		fprintf(stderr, TITLE ": Shader compilation error: %s.\n", info_log);
	}
#endif
	return result;
}

static unsigned int
compile_program(const char *vertex_shader, const char *fragment_shader)
{
	unsigned int
		vertex   = compile_shader(vertex_shader, GL_VERTEX_SHADER),
		fragment = compile_shader(fragment_shader, GL_FRAGMENT_SHADER),
		program  = glCreateProgram();
	glAttachShader(program, vertex);
	glAttachShader(program, fragment);
	glLinkProgram(program);
#ifdef DEBUG
	int success;
	char info_log[512];
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if(!success) {
		glGetProgramInfoLog(program, 512, NULL, info_log);
		fprintf(stderr, TITLE ": Program linking error: %s.\n", info_log);
	}
#endif
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	return program;
}

static void
key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	switch(key) {
	case GLFW_KEY_Q:
	case GLFW_KEY_ESCAPE:
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		break;
	case GLFW_KEY_ENTER:
		if(action != GLFW_PRESS) break;
		if(screenshot_taken++) {
			fprintf(stderr, TITLE ": Screenshot already taken.\n");
			break;
		}
		size_t size = (camera.wsize.x * camera.wsize.y) * 8;
		uint32_t w = camera.wsize.x, h = camera.wsize.y;
		short *data = malloc(size);
		if(!data) {
			fprintf(stderr, TITLE ": Unable to mallocate %lu bytes.\n", size);
			break;
		}
		w = ntohl(w), h = ntohl(h); // I hate my life
		glReadPixels(0, 0, camera.wsize.x, camera.wsize.y, GL_RGBA, GL_UNSIGNED_SHORT, data);
		#define WRITE(s, len) \
			if(write(1, (s), (len)) == (len)) \
				fprintf(stderr, TITLE ": error writing image to stdout.")
		WRITE("farbfeld", 8);
		WRITE((void*)&w,  4);
		WRITE((void*)&h,  4);
		WRITE(data,    size);
		#undef WRITE
		free(data);
		break;
	default:
		break;
	}
}

static void
error_callback(int error, const char *description)
{
	fprintf(stderr, "GLFW: %d: %s\n", error, description);
}

static void
framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	camera.wsize.x = width;
	camera.wsize.y = height;
	CamVec vsize = camera_get_viewport_size(&camera);
	camera_center(&camera, vsize);
	glViewport(camera.offset.x, camera.offset.y, vsize.x, vsize.y);
}

static void
cmd_parse(char *str, size_t len, Cmd *buf)
{
// FIXME: no boundry checks anywhere in this function!
#define IS_CONSONANT(c)\
		('a' <= c && c <= 'z')

#define CMD_MATCHES(cmd, cmd_size)\
		((i == cmd_size) && !strncmp(curr, cmd, cmd_size))

	size_t i = 0;
	char * curr = str;

	// defaults
	buf->colr = 1;
	buf->colg = 0.5;
	buf->colb = 0.2;
	buf->cola = 1;
	buf->rot  = 0;

	while(IS_CONSONANT(curr[i])) i += 1;
	if(CMD_MATCHES("ellipse", LEN_ELLIPSE_CMD))
		buf->t = ELLIPSE;
	else if(CMD_MATCHES("donut", LEN_DONUT_CMD))
		buf->t = DONUT;
	else if(CMD_MATCHES("rect", LEN_RECT_CMD))
		buf->t = RECT;
	else {
		buf->t = UNKNOWN_CMD;
		return;
	}

	curr += i;
	buf->x = strtod(curr, &curr);
	buf->y = strtod(curr, &curr);
	buf->r1 = strtod(curr, &curr);
	buf->r2 = strtod(curr, &curr);

	while(1) {
		i = 0;
		while(*curr == ' ') curr += 1;
		while(IS_CONSONANT(curr[i])) i += 1;
		if(CMD_MATCHES("color", LEN_COLOR_PROP)) {
			curr += i;
			buf->colr = strtod(curr, &curr);
			buf->colg = strtod(curr, &curr);
			buf->colb = strtod(curr, &curr);
			buf->cola = strtod(curr, &curr);
		} else if(CMD_MATCHES("rotate", LEN_ROTATE_PROP)) {
			curr += i;
			buf->rot = DEG_2_RAD(strtod(curr, &curr));
		} else if(curr[i] == ' ' || curr[i] == '\n' || curr[i] == '\0') {
			return;
		} else {
			buf->t = UNKNOWN_PROPERTY;
			return;
		}
	}
}

static void
cmds_load()
{
	cmd_count = 8;
	size_t i = 0, size = cmd_count * sizeof(Cmd);
	int line = 0;
	char buf[BUFSIZ], *p;
	if(!(cmds = malloc(size))) {
		fprintf(stderr, TITLE ": Unable to mallocate %lu bytes.\n", size);
		exit(1);
	}
	while(1) {
		while((p = fgets(buf, sizeof(buf), stdin))) { // eat consecutive empty lines
			line += 1;
			if(strcmp(buf, "\n") != 0 && buf[0] != '#')
				break;
		}
		if(!p) break;
		if((i+1) * sizeof(Cmd) >= size) {
			if(!(cmds = realloc(cmds, (size = (cmd_count *= 2) * sizeof(Cmd))))) {
				fprintf(stderr, TITLE ": Unable to reallocate %lu bytes.\n", size);
				exit(1);
			}
		}
		cmd_parse(buf, strlen(buf), cmds + i);
		if(cmds[i].t == UNKNOWN_CMD)
			fprintf(stderr, TITLE ": Unknown command at line %d.\n", line);
		else if(cmds[i].t == UNKNOWN_PROPERTY)
			fprintf(stderr, TITLE ": Unknown property at line %d.\n", line);
		else i += 1;
	}
	if(i * sizeof(Cmd) != size) {
		if(!(cmds = realloc(cmds, (size = (cmd_count = i) * sizeof(Cmd))))) {
			fprintf(stderr, TITLE ": Unable to reallocate %lu bytes.\n", size);
			exit(1);
		}
	}
}

static void
window_open()
{
	glfwInit();
	glfwSetErrorCallback(error_callback);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
	window = glfwCreateWindow(480, 600, TITLE, NULL, NULL);
	if(!window) {
		fprintf(stderr, TITLE ": Failed to open window.\n");
		exit(1);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
}

static void
gl_init()
{
	if(!gl_lite_init()) {
		fprintf(stderr, TITLE ": Failed to initialize OpenGL.\n");
		exit(1);
	}
	ellipse_program = compile_program(vertex_shader, ellipse_fragment_shader);
	donut_program = compile_program(vertex_shader, donut_fragment_shader);
	rect_program = compile_program(vertex_shader, rect_fragment_shader);
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, VERTICES_SIZE, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, COORDS_PER_VERTEX, GL_FLOAT, GL_FALSE, COORDS_PER_VERTEX * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0, 0, 480, 600);
}

static void
cleanup()
{
	glDeleteProgram(donut_program);
	glDeleteProgram(ellipse_program);
	glDeleteProgram(rect_program);
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glfwDestroyWindow(window);
	glfwTerminate();
	free(cmds);
}

static void
draw()
{
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	Cmd *cmd = cmds, *cmd_last = cmds + cmd_count;
	while(cmd < cmd_last) {
		mat4x4_identity(transf);
		mat4x4_translate_in_place(transf, cmd->x, cmd->y, 0);
		if(cmd->rot)
			mat4x4_rotate_Z(transf, transf, cmd->rot);
		vec4_scale(transf[0], transf[0], cmd->r1);
		switch(cmd->t) {
		case ELLIPSE:
			vec4_scale(transf[1], transf[1], cmd->r2);
			glUseProgram(ellipse_program);
			glUniform4f(glGetUniformLocation(ellipse_program, "col"), cmd->colr, cmd->colg, cmd->colb, cmd->cola);
			glUniformMatrix4fv(glGetUniformLocation(ellipse_program, "transform"), 1, GL_FALSE, *transf);
			break;
		case DONUT:
			vec4_scale(transf[1], transf[1], cmd->r1);
			glUseProgram(donut_program);
			float sqr2 = cmd->r2 / cmd->r1;
			sqr2 *= sqr2;
			glUniform4f(glGetUniformLocation(donut_program, "col"), cmd->colr, cmd->colg, cmd->colb, cmd->cola);
			glUniform1f(glGetUniformLocation(donut_program, "sq_r2"), sqr2); 
			glUniformMatrix4fv(glGetUniformLocation(donut_program, "transform"), 1, GL_FALSE, *transf);
			break;
		case RECT:
			vec4_scale(transf[1], transf[1], cmd->r2);
			glUseProgram(rect_program);
			glUniform4f(glGetUniformLocation(rect_program, "col"), cmd->colr, cmd->colg, cmd->colb, cmd->cola);
			glUniformMatrix4fv(glGetUniformLocation(rect_program, "transform"), 1, GL_FALSE, *transf);
			break;
		case UNKNOWN_CMD:
			fprintf(stderr, TITLE ": Unknown command.\n");
			break;
		case UNKNOWN_PROPERTY:
			fprintf(stderr, TITLE ": Unknown property.\n");
			break;
		default:
			fprintf(stderr, TITLE ": Wtf?.\n");
			break;
		}
		glDrawArrays(GL_TRIANGLE_FAN, 0, VERTEX_COUNT);
		cmd += 1;
	}
}

int
main(int argc, char **argv)
{
	cmds_load();
	window_open();
	gl_init();

	while(!glfwWindowShouldClose(window)) {
		draw();
		glfwSwapBuffers(window);
		glfwWaitEvents();
	}

	cleanup();
	return 0;
}

