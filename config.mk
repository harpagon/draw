# draw version
VERSION = 1

# Customize below to fit your system

# paths
#PREFIX = /usr/local
PREFIX = ${HOME}/.local
MANPREFIX = ${PREFIX}/share/man

# includes and libs
INCS = -I. -I/usr/include
LIBS = -ldl -lm -lpthread -lglfw3 -lGL -lX11

# flags
#CPPFLAGS = -DVERSION=\"${VERSION}\" -DDEBUG
#CFLAGS += -g -std=c99 -pedantic -Wall ${INCS} ${CPPFLAGS}
#LDFLAGS += -g -no-pie ${LIBS}
CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS += -std=c99 -pedantic -Wall -march=native -O3 ${INCS} ${CPPFLAGS}
LDFLAGS += ${LIBS}

# compiler and linker
CC ?= cc

