Possible improvements:
	1. Drawables
		a) new features
			- triangles
			- frame (rectangle with empty inside)
			- bezier curve
			- math functions
			- polygons

		b) redundant
			- circle
			- square
			- line (this would be actually great)

	2. Properties
		- textures

	3. Other
		- custom viewport size / ratio

	4. As separate project:
		- animations
		- 3d objects

